
ZendAMF Server v0.4
ZendAMF extension to the Services module

List of changes:

    * Changed from function-based gateway to a class-based one in order 
      to facilitate debugging. 
    * Implements a rudimentary key-based authentication scheme. 
      Note: i'm no security guru, i'm sure this will require actual encryption 
      down the line. In its current form, it's just a shared key system, likely 
      vulnerable to a man-in-the-middle attack; plus, i'm guessing the key 
      can be retrieved by decompiling the SWF. This authentication scheme does 
      act like your typical lock: should keep out most, but definitely 
      vulnerable to a few. 
    * Integrates ZamfBrowser for debugging (requires extra download).
    * Handles errors a little better now by throwing AS3-catchable errors 
      (still a work in progress)
    * Various bug fixes

To install:

    * Install the Services modules (dev)
    * Download the zendamf-7.x-0.4.tar.gz
    * Unpack in "sites/all/modules/services/servers" (this assumes "Services" 
      is already installed) or in "sites/all/modules/".
    * Download the latest ZendAMF, unpack and move the Zend folder ("library" 
      folder) to  "sites/all/libraries"
    * (optional) Download ZamfBrowser, unpack in the same spot (rename 
      folder "ZamfBrowser"). 
    * Create the service's endpoint (Admin > Configuration > Web Services 
      > Services)
    * Use 'zendamf' for everything for now.
    * Luckily, this should be it.

Heads up:

    * Don't forget to activate the resource you want to use
    * Don't forget to turn on debug if so required
    * Turn off ZendAMF Authentication on the endpoint if you rely a lot on 
      the external debugger. The authentication mechanism relies on AMF headers 
      to carry the key, and ZamfBrowser does not allow to add header to the 
      input call (at the time of writing).


-------
 Usage
-------

Technically, you should have a gateway going at http://your_site/zendamf, 
feel free to check. I just pust together a quick AS3 example in the ./as3
folder that calls the "node.retrieve" resource.

---------
 Warning
---------

I'm fully aware of my limitations as a programmer. I needed a 
Drupal-7-capable AMF gateway, there was none, so i hacked one and figured 
i'd share. This being said, it can't be _that_ bad: i've already on a handful 
of all-Flash website, and it seems to be doing OK. Anyway, can't say you 
haven't been warned. 


Jeff Gauthier, Obstacle
zeke@obstacle.com
