package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.Responder;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import DrupalNetConnection;

	/**
	 * ...
	 * @author Jeff Gauthier
	 */
	public class Main extends Sprite 
	{
		private var _text:TextField;
		private var nid:uint;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			__setupDisplay();
			
			tracer('Connecting to gateway...');
			
			nid = 1; //make sure this is a valid nid 
			
			var conn:DrupalNetConnection = new DrupalNetConnection();
			conn.connect('http://your_site/zendamf');
			//conn.userid = 'test';
			//conn.password = 'test';
			conn.call('node', 'retrieve', new Responder(__onSuccess, __onError), nid );
		}
		
		private function __onSuccess(o:Object):void{
			//Do something with that darn data 
			tracer('Success: You\'ve just loaded a node (nid:' + o.nid + ') titled "' + o.title + '"');
		}
		
		private function __onError(o:Object):void{
			// Scream!!!
			tracer(o, "##ERROR##");
		}
		
		private function __setupDisplay():void {
			addChild(_text = new TextField());
			_text.defaultTextFormat = new TextFormat('_sans', 9, 0x334477, true)
			_text.multiline = true;
			_text.wordWrap = true;
			_text.width = 400;
		}
		
		private function tracer(m:*, type:String = ""):void {
			var t:String;
			//yuck!
			if (m is String){
				t = m;
			}else{
				for (var i:String in m)
					t += i + " : " + m[i] + "\n";
			}
			trace(t);
			_text.text = t  ; 
			_text.autoSize = "left";
			_text.x = stage.stageWidth / 2 - 200
			var k:Number = stage.stageHeight / 2 - _text.height;
			_text.y = k > 0 ? k : 30; 
		}
		
	}
	
}

