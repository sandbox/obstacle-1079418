package  
{
	import flash.net.NetConnection;
	import flash.net.Responder;
	import flash.net.ObjectEncoding;
	/**
	 * ...
	 * @author Jeff Gauthier
	 */
	public class DrupalNetConnection extends NetConnection
	{
		public var userid:String;
		public var password:String;
		
		public function DrupalNetConnection() 
		{
			objectEncoding = ObjectEncoding.AMF3;
		}
		
		override public function call (module:String, action:String, responder:Responder, ...args) : void{
			var arr:Array = ["Drupal." + module + "__" + action, responder];
			if ( userid || password ) 
				addHeader('Credentials', false, { userid: userid, password: password })
			super.call.apply(null, arr.concat(args));
		}
		
	}

}
